﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ImageViewer.Resources
{
    public class ImageViewerResourceManager:ResourceManager
    {
        public ImageViewerResourceManager()
            :base("ImageViewer.Resources.ImageViewerResources", 
                 Assembly.GetExecutingAssembly()) { }
        
        public Bitmap GetImage(string name)
        {
            object ob = this.GetObject(name);
            return this.GetObject(name) as Bitmap;
        }
    }
}
