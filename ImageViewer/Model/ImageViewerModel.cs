﻿using ImageViewer.Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageViewer.Model
{
    public class ImageViewerModel
    {
        List<Bitmap> images = new List<Bitmap>();
        string programmTitle;
        string blurCaption;
        Bitmap favicon;

        public ImageViewerModel(ImageViewerResourceManager resourceManager)
        {
            initSampleData(resourceManager);
            blurCaption = resourceManager.GetString("BlurCaption");
            programmTitle = resourceManager.GetString("ProgramTitle");
            favicon = resourceManager.GetImage("favicon");
        }

        /// <summary>
        /// images list
        /// </summary>
        public List<Bitmap> Images
        {
            get { return images; }
            set { images = value; }
        }

        /// <summary>
        /// Title porperty
        /// </summary>
        public string ProgrammTitle
        {
            get { return programmTitle; }
        }

        /// <summary>
        /// blur property
        /// </summary>
        public string BlurCaption
        {
            get { return blurCaption; }
        }


        /// <summary>
        /// favicon porperty
        /// </summary>
        public Bitmap Favicon
        {
            get { return favicon; }
        }

        /// <summary>
        /// sample init
        /// </summary>
        /// <param name="resourceManager"></param>
        private void initSampleData(ImageViewerResourceManager resourceManager)
        {
            for (int i = 0; i < 11; i++)
            {
                images.Add(resourceManager.GetImage(string.Format("sample_{0}", i + 1)));
            }
        }
    }
}
