﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageViewer.View
{
    public static class AttachedDropBehavior
    {
        private static readonly DependencyProperty previewDropCommandProperty =
            DependencyProperty.RegisterAttached("PreviewDropCommand",
                        typeof(ICommand), typeof(AttachedDropBehavior),
                        new PropertyMetadata(PreviewDropCommandPropertyChangedCallBack)
                    );

        /// <summary>
        /// setter fo preview drop command
        /// </summary>
        /// <param name="element"></param>
        /// <param name="command"></param>
        public static void SetPreviewDropCommand(this UIElement element, ICommand command)
        {
            element.SetValue(previewDropCommandProperty, command);
        }

        /// <summary>
        /// getter for previev drop
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static ICommand GetPreviewDropCommand(UIElement element)
        {
            return (ICommand)element.GetValue(previewDropCommandProperty);
        }

        /// <summary>
        /// previe property changed
        /// </summary>
        /// <param name="frameworkElement"></param>
        /// <param name="inEventArgs"></param>
        private static void PreviewDropCommandPropertyChangedCallBack(
            DependencyObject frameworkElement, DependencyPropertyChangedEventArgs inEventArgs)
        {
            UIElement element = frameworkElement as UIElement;
            if (null == element) return;

            element.Drop += (sender, args) =>
            {
                IInputElement hitTestElement = element.InputHitTest(args.GetPosition(element));
                
                if (hitTestElement != null)
                {
                    object additionalData = null;
                    object originalSource = null;

                    if (hitTestElement is Border &&
                    (hitTestElement as Border).Child is ContentPresenter &&
                    ((hitTestElement as Border).Child as ContentPresenter).Content is Image)
                    {
                        additionalData = (((hitTestElement as Border).Child 
                        as ContentPresenter).Content as Image).Source;
                    }
                    else if (hitTestElement is Image)
                        additionalData = (hitTestElement as Image).Source;
                    else if (hitTestElement is ScrollViewer)
                        additionalData = findImage(element, args.GetPosition(element));
                    if (args.Effects == DragDropEffects.Move &&
                    args.Data.GetDataPresent(typeof(BitmapImage)))
                    {
                        originalSource = args.Data.GetData(typeof(BitmapImage));
                    }
                    object[] parameter = { args.Data, additionalData, originalSource };

                    GetPreviewDropCommand(element).Execute(parameter);
                    args.Handled = true;
                }
            };
        }

        #region find methods
        /// <summary>
        /// method to find nearest image
        /// </summary>
        /// <param name="element"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private static object findImage(UIElement element, Point point)
        {
            object left = findImageRight(element, point);
            if (left != null) return left;
            object down = findImageDown(element, point);
            if (down != null) return down;
            object leftDown = findImageRightDown(element, point);
            if (leftDown != null) return leftDown;
            return null;
        }

        /// <summary>
        /// method to find nearest right image
        /// </summary>
        /// <param name="element"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private static object findImageRight(UIElement element, Point point)
        {
            IInputElement rightHitTestElement = element.InputHitTest(new Point(point.X + 40, point.Y));
            return imageFinder(rightHitTestElement);
        }

        /// <summary>
        /// method to find nearest bottom image
        /// </summary>
        /// <param name="element"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private static object findImageDown(UIElement element, Point point)
        {
            IInputElement downHitTestElement = element.InputHitTest(new Point(point.X, point.Y + 40));
            return imageFinder(downHitTestElement);
        }

        /// <summary>
        /// method to find nearest bottom right image
        /// </summary>
        /// <param name="element"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        private static object findImageRightDown(UIElement element, Point point)
        {
            IInputElement leftDownHitTestElement = element.InputHitTest(new Point(point.X + 40, point.Y + 40));
            return imageFinder(leftDownHitTestElement);
        }

        /// <summary>
        /// analyzing images by type
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private static object imageFinder(IInputElement element)
        {
            if (element != null)
            {
                if (element is Border &&
                    (element as Border).Child is ContentPresenter &&
                    ((element as Border).Child as ContentPresenter).Content is Image)
                {
                    return (((element as Border).Child
                    as ContentPresenter).Content as Image).Source;
                }
                else if (element is Image)
                    return (element as Image).Source;
            }
            return null;
        }
        #endregion
    }
}



