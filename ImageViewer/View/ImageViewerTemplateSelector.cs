﻿using ImageViewer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ImageViewer.View
{
    class ImageViewerTemplateSelector:DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ImageViewerViewModel value = item as ImageViewerViewModel;

            if (value != null)
            {
                if (value.ViewMode == ViewMode.List)
                    return (container as FrameworkElement).FindResource("imageListTemplate")
                        as DataTemplate;
                else if (value.ViewMode == ViewMode.Single)
                    return (container as FrameworkElement).FindResource("singleImageTemplate")
                        as DataTemplate;
                return base.SelectTemplate(item, container);
            }
            else
                return base.SelectTemplate(item, container);
        }
    }
}
