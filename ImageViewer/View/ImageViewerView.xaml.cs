﻿using ImageViewer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageViewer.View
{
    /// <summary>
    /// Interaction logic for ImageViewerView.xaml 
    /// </summary>
    public partial class ImageViewerView : Window
    {
        public ImageViewerView(ImageViewerViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        /// <summary>
        /// update template by tile mouse double click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            presenter.ContentTemplateSelector = new ImageViewerTemplateSelector();
        }

        /// <summary>
        /// update template by esc press
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key== Key.Escape)
            {
                presenter.ContentTemplateSelector = new ImageViewerTemplateSelector();
            }
        }

        /// <summary>
        /// start drag for tiles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (sender is Button && (sender as Button).Content is Image)
                {                    
                    DragDrop.DoDragDrop(sender as DependencyObject, 
                        ((sender as Button).Content as Image).Source, 
                        DragDropEffects.Move);
                }

            }
        }
    }
}
