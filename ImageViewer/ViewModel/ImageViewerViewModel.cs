﻿using ImageViewer.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageViewer.ViewModel
{
    public class ImageViewerViewModel : NotifyingDependencyViewModel
    {
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);

        string[] extensions = { ".png", ".jpg", ".jpeg",".bmp",".jpg",".tiff",".tif", ".gif" };
        static object locker = new object();
        ImageViewerModel model;
        ImageViewerImage selectedImage;
        ViewMode viewMode;
        string programmTitle;
        string blurCaption;
        ImageSource appIcon;
        ObservableCollection<ImageViewerImage> images = new ObservableCollection<ImageViewerImage>();

        public ImageViewerViewModel(ImageViewerModel model)
        {
            this.model = model;
            viewMode = ViewMode.List;

            model.Images.AsParallel().WithMergeOptions(ParallelMergeOptions.FullyBuffered).ForAll(x => Images.Add(new ImageViewerImage(x)));
            

            programmTitle = model.ProgrammTitle;
            blurCaption = model.BlurCaption;
            var handle = model.Favicon.GetHbitmap();
            AppIcon = Imaging.CreateBitmapSourceFromHBitmap(handle, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromWidthAndHeight(32,32));
            DeleteObject(handle);
        }

        #region properties
        /// <summary>
        /// view mode of window
        /// </summary>
        public ViewMode ViewMode
        {
            get { return viewMode; }
            set
            {
                viewMode = value;
                OnPropertyChanged("ViewMode");

            }
        }

        /// <summary>
        /// image collection
        /// </summary>
        public ObservableCollection<ImageViewerImage> Images
        {
            get { return images; }
            set
            {
                images = value;
                OnPropertyChanged("Images");
            }
        }

        /// <summary>
        /// active element
        /// </summary>
        public ImageViewerImage SelectedImage
        {
            get { return selectedImage; }
            set
            {
                selectedImage = value;
                OnPropertyChanged("SelectedImage");
            }
        }

        /// <summary>
        /// application icon
        /// </summary>
        public ImageSource AppIcon
        {
            get { return appIcon; }
            set
            {
                appIcon = value;
                OnPropertyChanged("AppIcon");
            }
        }

        /// <summary>
        /// application title
        /// </summary>
        public string ProgrammTitle
        {
            get { return programmTitle; }
        }

        /// <summary>
        /// caption of blur button
        /// </summary>
        public string BlurCaption
        {
            get { return blurCaption; }
        }
        #endregion

        #region commands
        public ICommand ToSingleModeCommand
        {
            get
            {
                return new CommandWithParameter((parameter) =>
                {
                    ViewMode = ViewMode.Single;
                    var items = from ivi in images
                                where ivi.Image == parameter
                                select ivi;
                    SelectedImage = items.First();

                }
                );
            }
        }

        public ICommand NextImageCommand
        {
            get
            {
                return new CommandWithParameter((parameter) =>
                {
                    if (viewMode == ViewMode.Single && images != null && images.Count != 0)
                    {
                        if (images.IndexOf(selectedImage) == images.Count - 1)
                            SelectedImage = images.First();
                        else SelectedImage = images[images.IndexOf(selectedImage) + 1];
                    }
                });
            }
        }

        public ICommand PreviousImageCommand
        {
            get
            {
                return new CommandWithParameter((parameter) =>
                {
                    if (viewMode == ViewMode.Single && images != null && images.Count != 0)
                    {
                        if (images.IndexOf(selectedImage) == 0) SelectedImage = images.Last();
                        else SelectedImage = images[images.IndexOf(selectedImage) - 1];
                    }
                }
                );
            }

        }

        /// <summary>
        /// command to change view to list
        /// </summary>
        public ICommand BackToListCommand
        {
            get
            {
                return new CommandWithParameter((parameter) =>
                {
                    if (viewMode == ViewMode.Single)
                    {
                        ViewMode = ViewMode.List;
                    }
                }
                );
            }

        }

        /// <summary>
        /// drop command
        /// </summary>
        public ICommand DropCommand
        {
            get
            {
                return new CommandWithParameter((parameter) =>
                {
                    IDataObject data = (parameter as object[])[0] as IDataObject;
                    BitmapImage newImage = (parameter as object[])[1] as BitmapImage;
                    BitmapImage oldImage = (parameter as object[])[2] as BitmapImage;
                    if (viewMode == ViewMode.List && data != null)
                    {
                        if (data.GetDataPresent(DataFormats.FileDrop)) fileDrop(data, newImage);
                        else { imageDrop(data, newImage, oldImage); }
                    }
                });
            }
        }
        #endregion

        #region methods
        #region drop
        /// <summary>
        /// new files drop
        /// </summary>
        /// <param name="data"></param>
        /// <param name="image"></param>
        private void fileDrop(IDataObject data, BitmapImage image)
        {
            int index;
            if (image != null)
            {
                var items = from ivi in images
                            where ivi.Image == image
                            select ivi;
                index = images.IndexOf(items.First());
            }
            else index = images.Count;


            //Parallel.ForEach(((string[])data.GetData(DataFormats.FileDrop)).Where(x => extensions.Contains(Path.GetExtension(x))),
            //    x =>
            //    {
            //    lock (locker)
            //    {
            //        Dispatcher.Invoke(() =>
            //            Images.Insert(index++, new ImageViewerImage(new Bitmap(x))));
            //        };
            //    });
            //foreach (string fileName in ((string[])data.GetData(DataFormats.FileDrop)).
            //    Where(x => extensions.Contains(Path.GetExtension(x))).AsParallel())
            //{
            //    Images.Insert(index++, new ImageViewerImage(new Bitmap(fileName)));
            //}


            Debug.WriteLine($"start: {DateTime.Now}");
            foreach (string fileName in ((string[])data.GetData(DataFormats.FileDrop)).AsParallel()
                .Where(x => extensions.Contains(Path.GetExtension(x))))
            {
                Images.Insert(index++, new ImageViewerImage(new Bitmap(fileName)));
            }
            Debug.WriteLine($"end: {DateTime.Now}");
        }

        /// <summary>
        /// old images drop
        /// </summary>
        /// <param name="data"></param>
        /// <param name="newImage"></param>
        /// <param name="oldImage"></param>
        private void imageDrop(IDataObject data, BitmapImage newImage, BitmapImage oldImage)
        {
            if (oldImage != null)
            {
                
                var items = from ivi in images
                        where ivi.Image == oldImage
                        select ivi;
                int oldIndex = images.IndexOf(items.First());
                int newIndex;
                if (newImage != null)
                {
                    items = null;
                    items = from ivi in images
                            where ivi.Image == newImage
                            select ivi;
                    newIndex = images.IndexOf(items.First());
                }
                else newIndex = images.Count;           
                if (oldIndex == newIndex) return;
                if (newIndex == 0) { Images.Move(oldIndex, 0); }
                else { Images.Move(oldIndex, newIndex - 1); }
            }   
        }
        #endregion
        #endregion
    }
}
