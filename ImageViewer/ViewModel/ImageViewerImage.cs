﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace ImageViewer.ViewModel
{
    public class ImageViewerImage:NotifyingDependencyViewModel
    {
        BitmapImage image;
        int blurRadius = 0;
        string blurType = "Box";
        bool isBlured = false;
       
        public ImageViewerImage(Bitmap bitmap)
        {
            convertImage(bitmap);
        }

        private void convertImage(Bitmap bitmap)
        {
            using (System.IO.MemoryStream memory = new System.IO.MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                image = bitmapImage as BitmapImage;
            }
        }

        public ImageViewerImage(BitmapImage bitmap)
        {
            Image = bitmap;
        }

        #region propeties
            /// <summary>
            /// image
            /// </summary>
        public BitmapImage Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }
        }

        /// <summary>
        /// is blure active
        /// </summary>
        public bool IsBlured
        {
            get { return isBlured; }
            set
            {
                isBlured = value;
                OnPropertyChanged("IsBlured");
            }
        }

        /// <summary>
        /// blur radius
        /// </summary>
        public int BlurRadius
        {
            get { return blurRadius; }
            set
            {
                blurRadius = value;
                OnPropertyChanged("BlurRadius");
            }
        }

        /// <summary>
        /// blur type
        /// </summary>
        public string BlurType
        {
            get { return blurType; }
            set
            {
                blurType = value;
                OnPropertyChanged("BlurType");
            }
        }
        #endregion
    }
}
