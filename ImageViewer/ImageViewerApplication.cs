﻿using ImageViewer.Model;
using ImageViewer.Resources;
using ImageViewer.View;
using ImageViewer.ViewModel;
using System;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageViewer
{
    public class ImageViewerApplication:Application
    {
        static ImageViewerApplication app;

        [STAThread]
        public static void Main()
        {
            app = new ImageViewerApplication();
            app.Run();
        }

        public ImageViewerApplication()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");
            ImageViewerResourceManager res = new ImageViewerResourceManager();
            ImageViewerView window = new ImageViewerView(
                new ImageViewerViewModel(new ImageViewerModel(res)));
            window.Show();
        }
    }
}
