# Image Viewer #
## Test application for image viewing/draging/removing/bluring ##
### Functions list: ###
1. To add new images just drag and drop them to the window (multi drag and drop is available).
1. To change position of image drag and drop it to the new position.
1. To delete image from list press "x" button at the image corner.
1. To view image in detailed mode click it twice using left mouse button.
1. To blur/unblur detailed image use "Blur" check button.
1. To change image in detailed mode press UI buttons with arrows or press up/down keyboard buttons.